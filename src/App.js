
import React, {useEffect} from 'react';
import './App.scss';
import { ReactComponent as Logo } from './logo.svg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { faHeart, faSearch, faSave} from '@fortawesome/free-solid-svg-icons';


  
library.add(fab, faHeart, faSearch, faSave)

function App() {
const [result, setResult] = React.useState([]);
const [poke, setPoke] = React.useState([]);
const [load, setLoad] = React.useState('true');
const arr = [];

useEffect(() => {
  fetch('https://pokeapi.co/api/v2/pokemon/?limit=20')
  .then((response) => response.json())
  .then((data) => setResult(
  data.results.map((item) => {
  fetch(item.url)
  .then((response) => response.json())
  .then((allpokemon) => arr.push(allpokemon));
  setPoke(arr);
}),
));
}, []);
setTimeout(() => {
setLoad(false);
}, 1000);

return (
<div className="App">
    <section className="container">
        <div className='row'>
            <header className="col-12">
                <nav className="row navbar">
                    <div className="col-12 col-md-2"> 
                        <a className="navbar-brand col mb-3 mb-md-0 text-center" href="/"><Logo /></a>

                    </div>   
                    <div className="col-12 col-md-7">
                        <ul className="navbar-nav mr-auto d-lg-flex flex-lg-row d-md-flex flex-md-row justify-content-end">
                            <li className="nav-item active">
                                <a className="btnpok btnpok__primary" href="/">Todos los pokemon</a>
                            </li>
                            <li className="nav-item">
                                <a className="btnpok btnpok__primary" href="/favoritos"> <FontAwesomeIcon icon={['fas', 'heart']} />  <span>Favoritos</span></a>
                            </li>
                        </ul>
                    </div>
                    <div className="col-12 col-md-3 user-info mt-3 mt-md-0">
                        <div className="avatar">P</div>
                        <div className="user-data"><p>Hola Pablo</p></div>
                        <div className="user-fav"><span>Tienes <strong>20 Pokemones</strong></span> </div>
                    </div>
                </nav>
        
            </header>
            <div className="col-12 py-2 search-pok">
                <div class="form-inline">
                <FontAwesomeIcon icon={['fas', 'search']} /> <input class="form-control mr-sm-2" placeholder="Buscar" />
                </div>
            </div>
            <div className="col-12 back-img ">
                <div className="row">
                    <div className="col-3">

                    </div>
                    <div className="col-12 col-md-9 container-card mt-3">
                        <div className="row">
                        { load ? (
                            <div className="col spinner-pok">
                                <div className="d-flex align-items-center">
                                    <strong>Loading...</strong>
                                    <div className="spinner-border ml-auto" role="status" aria-hidden="true"></div>
                                </div>
                            </div>
                        ) : (
                        poke.map((img, i) => (
                            <div id={img.id} key={img.id} className="col-12 col-md-4">
                                 <div className='card card-pokemon mb-3'>
                                    <img className="card-pokemon__imgpokemon"  src={img.sprites.front_default} alt='pokemon' />
                                    <div>
                                    <h5 className="card-pokemon__title">{img.name}</h5>
                                    <h6 className="card-pokemon__subtitle">type: {img.types[0].type.name}</h6>
                                    </div>
                                    <div className="card-pokemon__link">
                                        <ul>
                                            <li>
                                                <a className="btnpok__link" href="#"><FontAwesomeIcon icon={['fas', 'heart']} />  Añadir a favoritos</a>
                                                <a className="btnpok__link" href="#"><FontAwesomeIcon icon={['fas', 'save']} />  Guardar</a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            ))
                        )}
                        </div>
                    </div>
                </div>
                
            </div>
        
        </div>
    </section>
    
</div>
);
}
export default App;